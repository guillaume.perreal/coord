package main

import (
	"fmt"
	"time"
)

type (
	ProcessState interface {
		Started(int) error
		Ended(int, string) error
		Errored(error) error
	}

	Event interface {
		Apply(ProcessState) error
	}

	StartedEvent struct {
		PID int
	}

	EndedEvent struct {
		ExitCode    int
		Description string
	}

	ErroredEvent struct{
		Error error
	}

	Emitter struct {
		*Conn
		Subject string
	}

	Receiver struct {
		PID      int
		ExitCode int
		EndDesc  string
		Error     error
	}

	ObservableProcessState struct {
		ProcessState
		Values chan ProcessState
		debouncer *time.Timer
	}
)

func (e StartedEvent) String() string {
	return fmt.Sprintf("{started, pid %d}", e.PID)
}

func (e StartedEvent) Apply(p ProcessState) error {
	return p.Started(e.PID)
}

func (e EndedEvent) Apply(p ProcessState) error {
	return p.Ended(e.ExitCode, e.Description)
}

func (e EndedEvent) String() string {
	return fmt.Sprintf("{ended, %s}", e.Description)
}

func (e ErroredEvent) Apply(p ProcessState) error {
	return p.Errored(e.Error)
}

func (e ErroredEvent) String() string {
	return fmt.Sprintf("{error, %s}", e.Error)
}

func (e *Emitter) emit(event Event) error {
	return e.Conn.Send(e.Subject, event)
}

func (e *Emitter) Started(PID int) error                 { return e.emit(StartedEvent{PID}) }
func (e *Emitter) Ended(exitCode int, desc string) error { return e.emit(EndedEvent{exitCode, desc}) }
func (e *Emitter) Errored(err error) error               { return e.emit(ErroredEvent{err}) }

func (r *Receiver) Started(PID int) error {
	r.ExitCode = -1
	r.EndDesc = ""
	r.PID = PID
	r.Error = nil
	return nil
}

func (r *Receiver) Ended(exitCode int, desc string) error {
	r.ExitCode = exitCode
	r.EndDesc = desc
	r.Error = nil
	return nil
}

func (r *Receiver) Errored(err error) error {
	r.Error = err
	return nil
}

func (r *Receiver) HasStarted() bool { return r.PID > 0 }
func (r *Receiver) HasEnded() bool { return r.HasStarted() && r.EndDesc != "" }
func (r *Receiver) IsSuccess() bool { return r.HasStarted() && r.ExitCode == 0 }

func Observe(p ProcessState) *ObservableProcessState {
	return &ObservableProcessState{
		ProcessState: p,
		Values: make(chan ProcessState, 5),
	}
}

func (o *ObservableProcessState) Started(pid int) error {
	err := o.ProcessState.Started(pid)
	if err == nil {
		o.debounce()
	}
	return err
}

func (o *ObservableProcessState) Ended(exitCode int, desc string) error {
	err := o.ProcessState.Ended(exitCode, desc)
	if err == nil {
		o.debounce()
	}
	return err
}

func (o *ObservableProcessState) Errored(error error) error {
	err := o.ProcessState.Errored(error)
	if err == nil {
		o.debounce()
	}
	return err
}

func (o *ObservableProcessState) Stop() {
	close(o.Values)
}

var ObservableDebounceTime = 50 * time.Millisecond

func (o *ObservableProcessState) debounce() {
	if o.debouncer != nil {
		o.debouncer.Reset(ObservableDebounceTime)
		return
	}
	o.debouncer = time.AfterFunc(ObservableDebounceTime, func() {
		o.Values <- o.ProcessState
		o.debouncer.Stop()
		o.debouncer = nil
	})
}
