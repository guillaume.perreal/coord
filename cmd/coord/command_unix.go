// +build !windows

package main

import (
	"os"
	"os/signal"
	"syscall"

	"gitlab.irstea.fr/guillaume.perreal/coord/lib/log"
)

func (c *Command) forwardSignals() {
	signals := make(chan os.Signal)
	signal.Notify(signals)

	defer func() {
		signal.Stop(signals)
		close(signals)
		log.Debug("stopped forwarding signals")
	}()

	log.Debug("starting forwarding signals")
	for {
		select {
		case <-c.ctx.Done():
			return
		case sig := <-signals:
			if sig == syscall.SIGCHLD {
				continue
			}
			if err := c.Cmd.Process.Signal(sig); err != nil {
				log.Warningf("could not forward signal %s: %s", sig, err)
			} else {
				log.Infof("forwarded signal %s", sig)
			}
		}
	}
}
