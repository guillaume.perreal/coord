package main

import (
	"encoding/json"
	"fmt"

	"github.com/nats-io/stan.go"

	"gitlab.irstea.fr/guillaume.perreal/coord/lib/log"
)

type (
	Conn struct {
		stan.Conn
		log.Logger
	}

	eventUnion struct {
		Started *StartedEvent
		Ended   *EndedEvent
		Error   *ErroredEvent
	}
)

func NewConn(clusterID string, clientId string, url string) (*Conn, error) {
	logger := log.DefaultLogger.WithField("conn", fmt.Sprintf("%s[%s]", url, clusterID))

	logger.Debugf("connecting as %q", url, clusterID, clientId)
	conn, err := stan.Connect(clusterID, clientId, stan.NatsURL(url))
	if err != nil {
		return nil, err
	}
	logger.Infof("connected as %q", conn.NatsConn().ConnectedUrl(), clusterID, clientId)

	return &Conn{conn, logger}, nil
}

func (c *Conn) Send(subject string, event Event) error {
	logger := c.Logger.WithField("channel", subject)

	var data eventUnion
	switch concreteEvent := event.(type) {
	case StartedEvent:
		data.Started = &concreteEvent
	case EndedEvent:
		data.Ended = &concreteEvent
	case ErroredEvent:
		data.Error = &concreteEvent
	default:
		return fmt.Errorf("cannot send %#v", event)
	}

	if bytes, err := json.Marshal(data); err != nil {
		return err
	} else if err := c.Conn.Publish(subject, bytes); err != nil {
		return err
	}
	logger.Debugf("sent event %s", event)
	return nil
}

func (c *Conn) Subscribe(subject string, handler func(event Event), opts ...stan.SubscriptionOption) (stan.Subscription, error) {
	logger := c.Logger.WithField("channel", subject)

	stanHandler := func(msg *stan.Msg) {
		var data eventUnion
		if err := json.Unmarshal(msg.Data, &data); err != nil {
			logger.Errorf("could not unmarshal data `%s`: %s", msg.Data, err)
			return
		}

		var event Event
		if data.Started != nil {
			event = *data.Started
		} else if data.Ended != nil {
			event = *data.Ended
		} else if data.Error != nil {
			event = *data.Error
		} else {
			logger.Errorf("cannot handle %#v", data)
			return
		}

		logger.Debugf("received event: %s", event)
		handler(event)
	}

	return c.Conn.Subscribe(subject, stanHandler, opts...)
}
