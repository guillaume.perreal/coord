package main

import (
	"context"
	"errors"
	"os"
	"os/exec"
	"strings"

	"gitlab.irstea.fr/guillaume.perreal/coord/lib/log"
)

type (
	Command struct {
		*exec.Cmd
		ctx context.Context
		cancel func()
	}
)

func NewCommand(command []string) (*Command, error) {
	if len(command) < 1 {
		return nil, errors.New("a command is required")
	}

	cmdName := command[0]
	arguments := command[1:]

	cmdPath, err := exec.LookPath(cmdName)
	if err != nil {
		return nil, err
	}

	ctx, cancel := context.WithCancel(context.Background())

	cmd := exec.CommandContext(ctx, cmdPath, arguments...)
	cmd.Stdin, cmd.Stdout, cmd.Stderr = os.Stdin, os.Stdout, os.Stderr

	return &Command{cmd, ctx, cancel}, nil
}

func (c *Command) String() string {
	return strings.Join(c.Args, " ")
}

func (c *Command) Start() error {
	err := c.Cmd.Start()
	if err != nil {
		return err
	}
	log.Noticef("started command `%s`, PID: %d", c, c.Process.Pid)

	go c.forwardSignals()

	return nil
}

func (c *Command) Wait() error {
	defer func() {
		c.cancel()
		log.Noticef("command terminated: %s", c.Cmd.ProcessState)
	}()
	return c.Cmd.Wait()
}
