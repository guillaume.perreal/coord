package main

import (
	"crypto/sha1"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strings"
	"sync"

	"github.com/nats-io/nats.go"
	"github.com/nats-io/stan.go"
	"github.com/spf13/pflag"
	"github.com/spf13/viper"

	"gitlab.irstea.fr/guillaume.perreal/coord/lib/log"
)

func main() {
	if err := Run(); err != nil {
		log.Critical(err.Error())
		os.Exit(1)
	}
}

func Run() error {
	pflag.StringP("url", "u", nats.DefaultURL, "URL of the NATS server")
	pflag.StringP("clusterID", "c", "COORD", "Cluster ID of the NATS Streaming server")
	pflag.StringP("commandID", "i", "", "Identifier of this command")
	pflag.StringSliceP("wait", "w", nil, "Command ID to wait for")
	pflag.StringP("durable", "d", "", "persistent identifier for replay")
	pflag.Parse()

	if err := viper.BindPFlags(pflag.CommandLine); err != nil {
		return err
	}

	viper.SetEnvPrefix("coord")
	viper.AutomaticEnv()

	viper.SetConfigName(".coord")
	viper.AddConfigPath(".")
	if xdgConfig := os.Getenv("XDG_CONFIG_HOME"); xdgConfig != "" {
		viper.AddConfigPath(xdgConfig)
	} else if home, err := os.UserHomeDir(); err == nil {
		viper.AddConfigPath(filepath.Join(home, ".config"))
	}

	if err := viper.ReadInConfig(); err != nil {
		if _, notFound := err.(viper.ConfigFileNotFoundError); notFound {
			log.Info(err.Error())
		} else {
			return err
		}
	}

	cmd, err := NewCommand(pflag.Args())
	if err != nil {
		return err
	}

	hostname, err := os.Hostname()
	if err != nil {
		return err
	}

	hash := sha1.New()
	hash.Write([]byte(strings.Join(cmd.Args, " ")))
	defaultCommandID := fmt.Sprintf("coord.cmd.%x", hash.Sum(nil)[:8]);

	hash.Write([]byte(hostname))
	defaultClientID := fmt.Sprintf("coord-%x", hash.Sum(nil)[:8])

	viper.SetDefault("commandID", defaultCommandID)
	viper.SetDefault("durable", defaultClientID)

	log.DefaultLogger = log.DefaultLogger.WithField("id", viper.GetString("commandID"))

	conn, err := NewConn(
		viper.GetString("clusterID"),
		viper.GetString("durable"),
		viper.GetString("url"),
	)
	if err != nil {
		return err
	}
	defer CloseLog(conn)

	if waitFor := viper.GetStringSlice("wait"); len(waitFor) > 0 {
		opts := []stan.SubscriptionOption{
			stan.DurableName(viper.GetString("durable")),
		}

		before := sync.WaitGroup{}
		for _, subject := range waitFor {
			subject := subject
			state := Observe(&Receiver{})
			defer state.Stop()

			sub, err := conn.Subscribe(subject, func(event Event) {
				LogError(event.Apply(state))
			}, opts...)

			if err != nil {
				return err
			}

			before.Add(1)
			defer CloseLog(sub)

			go func() {
				for value := range state.Values {
					if st, ok := value.(*Receiver); ok && st.IsSuccess() {
						log.Debugf("%q succeeded", subject)
						before.Done()
						return
					}
				}
			}()

			log.Noticef("waiting for %q to succeed", subject)
		}

		before.Wait()
	}

	process := &Emitter{conn, viper.GetString("commandID")}

	err = cmd.Start()
	if err != nil {
		LogError(process.Errored(err))
	} else {
		defer func() {
			_ = cmd.Process.Kill()
			_, _ = cmd.Process.Wait()
		}()
		LogError(process.Started(cmd.Process.Pid))
	}

	err = cmd.Wait()
	if err != nil {
		LogError(process.Errored(err))
	} else {
		LogError(process.Ended(cmd.ProcessState.ExitCode(), cmd.ProcessState.String()))
	}

	return nil
}

//
//func waitFor(conn *Conn, channel string, wg *sync.WaitGroup, opts []stan.SubscriptionOption) (sub stan.Subscription, err error) {
//	var once sync.Once
//
//	sub, err = conn.Subscribe(channel, func(msg *stan.Msg) {
//		log.Debugf("received message %v", msg)
//		var event struct {
//			*StartedEvent
//			*ErrorEvent
//			*TerminatedEvent
//		}
//		if err := json.Unmarshal(msg.Data, &event); err != nil {
//			log.Debugf("could not unmarshal to terminated event: %s", msg)
//			return
//		}
//		if event.TerminatedEvent == nil {
//			return
//		}
//		log.Debugf("%s terminated: %v", channel, event)
//		if event.Success {
//			once.Do(func() {
//				log.Debugf("%s succeeded", channel)
//				wg.Done()
//			})
//		}
//	}, opts...)
//
//	if err == nil {
//		wg.Add(1)
//	}
//
//	return
//}

func LogError(err error) {
	if err != nil {
		log.Notice(err.Error())
	}
}

func DoLog(f func() error) {
	LogError(f())
}

func CloseLog(closer io.Closer) {
	LogError(closer.Close())
}

func Squelch(f func () error) {
	_ = f()
}
