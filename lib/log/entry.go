package log

import (
	"fmt"
	"os"
	"path/filepath"
	"time"
)

// An Entry holds information something to report to the user.
type Entry struct {
	time.Time
	Category string
	Level
	Message string
	*Fields
	PID int
}

// The DefaultCategory is used to set the category of newly created Entries.
var DefaultCategory = filepath.Base(os.Args[0])

// NewEntry creates a New entry at the given level and with the given message, the default category and no fields.
func NewEntry(level Level, message string) *Entry {
	return &Entry{time.Now(), DefaultCategory, level, message, nil, os.Getpid()}
}

// NewEntryf creates a New entry at the given level and with the given message, the default category and no fields.
func NewEntryf(level Level, template string, values ...interface{}) *Entry {
	return NewEntry(level, fmt.Sprintf(template, values...))
}
