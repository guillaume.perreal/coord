package log

import (
	"encoding/json"
	"fmt"
	"strings"
)

// Fields is a linked list of (name, value) couple
type Fields struct {
	name     string
	value    interface{}
	previous *Fields
}

// With creates new Fields that include the given (name, value) couple.
func (f *Fields) With(name string, value interface{}) *Fields {
	return &Fields{name, value, f}
}

// ForEach applies the given function for each (name, value) couple, as long as it returns true.
func (f *Fields) ForEach(do func(string, interface{}) bool) {
	if f != nil && do(f.name, f.value) {
		f.previous.ForEach(do)
	}
}

// IsEmpty returns true if there are no fields.
func (f *Fields) IsEmpty() bool {
	return f == nil
}

// Len returns the number of fields.
func (f *Fields) Len() int {
	if f == nil {
		return 0
	}
	return 1 + f.previous.Len()
}

// Map returns the Fields as a map.
func (f *Fields) Map() map[string]interface{} {
	fieldMap := make(map[string]interface{}, f.Len())
	f.ForEach(func(key string, value interface{}) bool {
		fieldMap[key] = value
		return true
	})
	return fieldMap
}

// String implements io.Stringer.
func (f *Fields) String() string {
	var buffer strings.Builder
	first := true
	f.ForEach(func(key string, value interface{}) bool {
		if !first {
			_, _ = buffer.WriteRune(' ')
		} else {
			first = false
		}
		_, _ = fmt.Fprintf(&buffer, "%s=%s", key, value)
		return true
	})
	return buffer.String()
}

// MarshalJSON implements json.Marshaler
func (f *Fields) MarshalJSON() ([]byte, error) {
	return json.Marshal(f.Map())
}
