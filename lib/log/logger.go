package log

import "io"

// EntryLogger is the building brick of the package.
type EntryLogger interface{ LogEntry(*Entry) }

// EntryLoggerFunc is a EntryLogger implementation using a function.
type EntryLoggerFunc func(*Entry)

// LogEntry implements EntryLogger
func (f EntryLoggerFunc) LogEntry(entry *Entry) { f(entry) }

// Logger adds convenience methods to a EntryLogger.
type Logger struct{ EntryLogger }

// NewLogger wraps an EntryLogger to provide convenience methods.
func NewLogger(downstream EntryLogger) Logger {
	return Logger{downstream}
}

// Log creates and logs an Entry as the given level and with the given message.
func (l Logger) Log(level Level, message string) { l.LogEntry(NewEntry(level, message)) }

// Debug creates and logs an Entry of Debug Level.
func (l Logger) Debug(message string) {
	l.Log(DEBUG, message)
}

// Info creates and logs an Entry of Info level.
func (l Logger) Info(message string) {
	l.Log(INFO, message)
}

// Notice creates and logs an Entry of Notice level.
func (l Logger) Notice(message string) {
	l.Log(NOTICE, message)
}

// Warning creates and logs an Entry of Warning level.
func (l Logger) Warning(message string) {
	l.Log(WARNING, message)
}

// Error creates and logs an Entry of Error level.
func (l Logger) Error(message string) {
	l.Log(ERROR, message)
}

// Critical creates and logs an Entry of Critical level.
func (l Logger) Critical(message string) {
	l.Log(CRITICAL, message)
}

// Logf creates and logs an Entry of the given level and with a message created from the given template and values.
func (l Logger) Logf(level Level, template string, values ...interface{}) {
	l.LogEntry(NewEntryf(level, template, values...))
}

func (l Logger) Debugf(template string, values ...interface{})   { l.Logf(DEBUG, template, values...) }
func (l Logger) Infof(template string, values ...interface{})    { l.Logf(INFO, template, values...) }
func (l Logger) Noticef(template string, values ...interface{})  { l.Logf(NOTICE, template, values...) }
func (l Logger) Warningf(template string, values ...interface{}) { l.Logf(WARNING, template, values...) }
func (l Logger) Errorf(template string, values ...interface{})   { l.Logf(ERROR, template, values...) }
func (l Logger) Criticalf(template string, values ...interface{}) {
	l.Logf(CRITICAL, template, values...)
}

// WithField creates a new Logger that adds the given field to every entries.
func (l Logger) WithField(name string, value interface{}) Logger {
	return Logger{EntryLoggerFunc(func(Entry *Entry) {
		Entry.Fields = Entry.Fields.With(name, value)
		l.LogEntry(Entry)
	})}
}

// WithCategory creates a new Logger that sets the category of every entries.
func (l Logger) WithCategory(category string) Logger {
	return Logger{EntryLoggerFunc(func(Entry *Entry) {
		Entry.Category = category
		l.LogEntry(Entry)
	})}
}

// A Writer is a EntryLogger that filters, formats and writes entries to an io.Writer.
type Writer struct {
	minLevel  Level
	writer    io.Writer
	formatter Formatter
}

// NewWriter creates a new Writer
func NewWriter(minLevel Level, writer io.Writer, formatter Formatter) *Writer {
	return &Writer{minLevel, writer, formatter}
}

// LogEntry implements EntryLogger.
func (w *Writer) LogEntry(Entry *Entry) {
	if Entry.Level < w.minLevel {
		return
	}
	if bytes, err := w.formatter.Format(Entry); err != nil {
		errorHandler(err)
	} else if _, err := w.writer.Write(bytes); err != nil {
		errorHandler(err)
	}
}
