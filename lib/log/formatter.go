package log

import (
	"fmt"
	"strings"
	"time"
)

// A Formatter formats an entry.
type Formatter interface {
	Format(*Entry) ([]byte, error)
}

// A FormatterFunc implements Formatter using a single function.
type FormatterFunc func(*Entry) ([]byte, error)

// Format implements Formatter.
func (f FormatterFunc) Format(entry *Entry) ([]byte, error) { return f(entry) }

// TextFormatter formats an entry as one-line text.
var TextFormatter = FormatterFunc(func(entry *Entry) ([]byte, error) {
	return []byte(strings.TrimSpace(fmt.Sprintf(
		"%s %s[%d] [%s]: %s {%s}",
		entry.Time.Format(time.RFC3339),
		entry.Category,
		entry.PID,
		entry.Level.String()[:4],
		entry.Message,
		entry.Fields,
	)) + "\n"), nil
})
