package log

import "context"

type loggerContextKeyType int

const loggerContextKey loggerContextKeyType = 0

func WithLogger(ctx context.Context, logger EntryLogger) context.Context {
	return context.WithValue(ctx, loggerContextKey, logger)
}

func FromContext(ctx context.Context) Logger {
	if logger, ok := ctx.Value(loggerContextKey).(Logger); ok {
		return logger
	} else if entryLogger, ok := ctx.Value(loggerContextKey).(EntryLogger); ok {
		return Logger{entryLogger}
	}
	return DefaultLogger
}
