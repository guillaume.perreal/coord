package log

import (
	"os"
)

// DefaultLogger is the package logger.
var DefaultLogger = NewLogger(NewWriter(DEBUG, os.Stderr, TextFormatter))

// Debug logs a message at DEBUG level using the package logger.
func Debug(message string)    { DefaultLogger.Debug(message) }
func Info(message string)     { DefaultLogger.Info(message) }
func Notice(message string)   { DefaultLogger.Notice(message) }
func Warning(message string)  { DefaultLogger.Warning(message) }
func Error(message string)    { DefaultLogger.Error(message) }
func Critical(message string) { DefaultLogger.Critical(message) }

func Debugf(template string, args ...interface{})    { DefaultLogger.Debugf(template, args...) }
func Infof(template string, args ...interface{})     { DefaultLogger.Infof(template, args...) }
func Noticef(template string, args ...interface{})   { DefaultLogger.Noticef(template, args...) }
func Warningf(template string, args ...interface{})  { DefaultLogger.Warningf(template, args...) }
func Errorf(template string, args ...interface{})    { DefaultLogger.Errorf(template, args...) }
func Criticalf(template string, args ...interface{}) { DefaultLogger.Criticalf(template, args...) }

// The errorHandler is used when something goes wrong in a logger.
var errorHandler = defaultErrorHandler

// SetErrorHandler changes the error handler.
func SetErrorHandler(new func(err error)) (old func(err error)) {
	errorHandler, old = new, errorHandler
	return
}

func defaultErrorHandler(err error) {
	_, _ = os.Stderr.WriteString("Error while logging: " + err.Error() + "\n")
}
