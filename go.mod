module gitlab.irstea.fr/guillaume.perreal/coord

go 1.12

require (
	github.com/hashicorp/go-hclog v0.9.2 // indirect
	github.com/hashicorp/go-immutable-radix v1.1.0 // indirect
	github.com/hashicorp/go-uuid v1.0.1 // indirect
	github.com/hashicorp/golang-lru v0.5.1 // indirect
	github.com/nats-io/jwt v0.2.8 // indirect
	github.com/nats-io/nats-streaming-server v0.15.1 // indirect
	github.com/nats-io/nats.go v1.8.1
	github.com/nats-io/stan.go v0.5.0
	github.com/prometheus/procfs v0.0.3 // indirect
	github.com/spf13/pflag v1.0.3
	github.com/spf13/viper v1.4.0
	go.etcd.io/bbolt v1.3.3 // indirect
	golang.org/x/crypto v0.0.0-20190621222207-cc06ce4a13d4 // indirect
	golang.org/x/sys v0.0.0-20190624142023-c5567b49c5d0 // indirect
	google.golang.org/appengine v1.6.1 // indirect
)
