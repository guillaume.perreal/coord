#!/usr/bin/env bash
set -ex

go build -v ./cmd/coord/

export NATS_URL=nats://127.0.0.1:4222
export NATS_CLUSTER_ID=COORD

./coord -i LS ls &

./coord -w LS echo bla

wait

